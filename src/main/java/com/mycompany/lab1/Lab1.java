/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab1 {

    public static void main(String[] args) {
        // Scanner to can Input
        Scanner kb = new Scanner(System.in) ;

        // InGame //
            boolean InGame = true ;
            boolean Process = true ;
            int AreaRow ;
            int AreaCol ;
            boolean Replay = true ;

                    //ReGame ***
            while (Replay == true){
                // Create Table
                int rows = 3;
                int columns = 3;
                String[][] array = new String[rows][columns];
        
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < columns; j++) {
                        array[i][j] = "-";
                    }
                }
        
                System.out.println("The Table");
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < columns; j++) {
                        System.out.print(array[i][j] + " ");
                    }
                    System.out.println();
                }

                System.out.print("Choose the starting side(O,X) : ") ;
                String Pick = kb.next();
                System.out.println();

                int countGame = 0;
                    //Play Game ***
            while (InGame == true){
            // Game Each side
            if (Pick.equals("O")  || Pick.equals("o")) {

                // O turn

                    // Exception When type wrong
                while (Process == true) {
                    try {
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < columns; j++) {
                        System.out.print(array[i][j] + " ");
                    }
                System.out.println();
                }
                System.out.println("Input Area -> row column ") ;
                System.out.print("O turn : ") ;
                
                AreaRow = kb.nextInt();
                AreaCol = kb.nextInt();
                array[AreaRow-1][AreaCol-1] = "O" ;
                System.out.println();
                countGame++ ;
                break ;
                } catch (ArrayIndexOutOfBoundsException e){
                        System.out.println();
                        System.out.println("Your Row or Column was wrong!! ");
                        System.out.println("Please type Row and Column again...");
                }

                // catch (InputMismatchException e){
                //         System.out.println();
                //         System.out.println("Input Only number!! ");
                //         System.out.println("Please type Row and Column again...");
                // }

                }

                    // Check Game
                //CheckGameForO(array, rows, columns, InGame);
                InGame = CheckGameForO(array, rows, columns, InGame);
                // End Game
                if (InGame == false){
                    // Stop Game
                    break ;
                }
                // Draw
                if (countGame == 9){
                    ShowTable(rows, columns, array);
                    System.out.println("Draw!!!");
                    break ;
                }

                // End Game
                if (InGame == false){
                    // Stop Game
                    break ;
                }
                    
                // ---------------------------------------------
                
                // X turn
                while (Process == true) {
                    try {
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < columns; j++) {
                        System.out.print(array[i][j] + " ");
                }
                System.out.println();
             }  

                System.out.println("Input Area -> row column ") ;
                System.out.print("X turn : ") ;
                AreaRow = kb.nextInt();
                AreaCol = kb.nextInt();
                array[AreaRow-1][AreaCol-1] = "X" ;
                System.out.println();
                countGame++ ;
                break ;
                } catch (ArrayIndexOutOfBoundsException e){
                        System.out.println();
                        System.out.println("Your Row or Column was wrong!! ");
                        System.out.println("Please type Row and Column again...");
                }
            }

                    // Check Game
                //CheckGameForX(array, rows, columns, InGame);
                InGame = CheckGameForX(array, rows, columns, InGame);
                // End Game
                if (InGame == false){
                    // Stop Game
                    break ;
                }
                // Draw
                if (countGame == 9){
                    ShowTable(rows, columns, array);
                    System.out.println("Draw!!!");
                    break ;
                }
            
            } else if (Pick.equals("X")  || Pick.equals("x")) {
                // X turn
                 // Exception When type wrong
                while (Process == true) {
                    try {
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < columns; j++) {
                        System.out.print(array[i][j] + " ");
                }
                System.out.println();
                }  

                System.out.println("Input Area -> row column ") ;
                System.out.print("X turn : ") ;
                AreaRow = kb.nextInt();
                AreaCol = kb.nextInt();
                array[AreaRow-1][AreaCol-1] = "X" ;
                System.out.println();
                countGame++ ;
                break ;
                } catch (ArrayIndexOutOfBoundsException e){
                        System.out.println();
                        System.out.println("Your Row or Column was wrong!! ");
                        System.out.println("Please type Row and Column again...");
                }
            }
                    // Check Game
                //CheckGameForX(array, rows, columns, InGame);
                InGame = CheckGameForX(array, rows, columns, InGame);
                // End Game
                if (InGame == false){
                    // Stop Game
                    break ;
                }
                // Draw
                if (countGame == 9){
                    ShowTable(rows, columns, array);
                    System.out.println("Draw!!!");
                    break ;
                }

                // End Game
                if (InGame == false){
                    // Stop Game
                    break ;
                }
                
                // ---------------------------------------------
                
                // O turn
                     // Exception When type wrong
                while (Process == true) {
                    try {
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < columns; j++) {
                        System.out.print(array[i][j] + " ");
                    }
                System.out.println();
                }
                System.out.println("Input Area -> row column ") ;
                System.out.print("O turn : ") ;
                AreaRow = kb.nextInt();
                AreaCol = kb.nextInt();
                array[AreaRow-1][AreaCol-1] = "O" ;
                System.out.println();
                countGame++ ;
                break ;
                } catch (ArrayIndexOutOfBoundsException e){
                        System.out.println();
                        System.out.println("Your Row or Column was wrong!! ");
                        System.out.println("Please type Row and Column again...");
                }
            }
                    // Check Game

                //CheckGameForO(array, rows, columns, InGame);
                InGame = CheckGameForO(array, rows, columns, InGame);
                // End Game
                if (InGame == false){
                    // Stop Game
                    break ;
                }
                // Draw
                if (countGame == 9){
                    ShowTable(rows, columns, array);
                    System.out.println("Draw!!!");
                    break ;
                }


            } else {
                System.out.println("Please enter the correct side...") ;
                System.out.print("Choose the starting side(O,X) : ") ;
                Pick = kb.next();
                System.out.println();
            
            }

        }

        // Replay Game
                System.out.print("Replay?(y/n) : ");
                String AnsToReplay = kb.next();
                System.out.println();
                if (AnsToReplay.equals("Y") || AnsToReplay.equals("y")){
                InGame = true ;
                Replay = true ;
            } else  if (AnsToReplay.equals("N") || AnsToReplay.equals("n")){
                System.out.println("Good Luck...");
                System.out.println();
                Replay = false ;
            } else {
                Replay = false ;
            }
        
        }
            

    }

    //Function Table
    static void ShowTable(int rows , int columns , String[][] array) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }

    //Function Check win of O side
    static boolean CheckGameForO(String[][] array, int rows , int columns , boolean InGame){
        for (int i = 0; i < 3 ; i++){
                    // Win in Rows for O side
                    if (array[i][0].equals("O") && array[i][1].equals("O") && array[i][2].equals("O")){
                        ShowTable(rows, columns, array);
                        System.out.println("O side Win!!!") ;
                        return InGame = false ;
                    }
                    // Win in Columns for O side
                    else if (array[0][i].equals("O") && array[1][i].equals("O") && array[2][i].equals("O")){
                        ShowTable(rows, columns, array);
                        System.out.println("O side Win!!!") ;
                        return InGame = false ;
                    }
                }
                    // Win in Diagonally \ for O side
                if (array[0][0].equals("O") && array[1][1].equals("O") && array[2][2].equals("O")){
                    ShowTable(rows, columns, array);
                    System.out.println("O side Win!!!") ;
                    InGame = false ;
                }
                // Win in Diagonally / for O side
                else if (array[0][2].equals("O") && array[1][1].equals("O") && array[2][0].equals("O")){
                    ShowTable(rows, columns, array);
                    System.out.println("O side Win!!!") ;
                    InGame = false ;
                }
        return InGame ;
    }

    //Function Check win of O side
    static boolean CheckGameForX(String[][] array, int rows , int columns , boolean InGame){
        for (int i = 0; i < 3 ; i++){
                    // Win in Rows for X side
                    if (array[i][0].equals("X") && array[i][1].equals("X") && array[i][2].equals("X")){
                        ShowTable(rows, columns, array);
                        System.out.println("X side Win!!!") ;
                        return InGame = false ;
                    }
                    // Win in Colums for X side
                    else if (array[0][i].equals("X") && array[1][i].equals("X") && array[2][i].equals("X")){
                        ShowTable(rows, columns, array);
                        System.out.println("X side Win!!!") ;
                        return InGame = false ;
                    }
                }
                // Win in Diagonally \ for X side
                if (array[0][0].equals("X") && array[1][1].equals("X") && array[2][2].equals("X")){
                    ShowTable(rows, columns, array);
                    System.out.println("X side Win!!!") ;
                    InGame = false ;
                }
                // Win in Diagonally / for X side
                else if (array[0][2].equals("X") && array[1][1].equals("X") && array[2][0].equals("X")){
                    ShowTable(rows, columns, array);
                    System.out.println("X side Win!!!") ;
                    InGame = false ;
                }
        return InGame ;
    }
}

